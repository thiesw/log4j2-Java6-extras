## Some extras for log4j2 2.3 still running on Java 6

- JUL-Handler as bridge from JUL to log4j2 - as alternative to JUL-LogManager, useful for webapps.
- Extended STMP appender with pattern in subject and burst handling.
- backport of `%notEmpty` and `%maxLen` for Java 6 (see [orig log4j 2.x doc](https://logging.apache.org/log4j/2.x/manual/layouts.html#PatternLayout) in *Patterns* table at *maxLen* and *variablesNotEmpty*).

This library is available at [Maven Central](https://search.maven.org/search?q=g:de.it-tw AND a:log4j2-Java6-extras): `de.it-tw:log4j2-Java6-extras`.


#### Usage BridgeHandler

You also need `log4j-jul.jar` dependency (*without* using its JUL-LogManager via -D..., simply the jar in WEB-INF/lib).

In Tomcat (and maybe other servers, too), simply create a
`WEB-INF/classes/logging.properties` with<br>
`handlers = org.apache.logging.log4j.jul.Log4jBridgeHandler`<br>
(and typically `org.apache.logging.log4j.jul.Log4jBridgeHandler.propagateLevels = true`)<br>
and JUL logs go to log4j2.

Alternatively you may call `Log4jBridgeHandler.install()` inside your webapps initialisation.

See also JavaDoc inside:
- [Log4jBridgeHandler](src/main/java/org/apache/logging/log4j/jul/Log4jBridgeHandler.java)
- Unit test: [implementation](src/test/java/org/apache/logging/log4j/jul/Log4jBridgeHandlerTest.java) and its [configuration](src/test/resources)



#### Usage SmtpAppender

The appender is like the known SmtpAppender.

But it allows a PatternLayout-pattern as subject (newer version of lo4j2 also include this feature). You have to activate this with `subjectWithLayout="true"` in the log4j-config.

Burst summarizing is useful to not get your inbox filled up with automatically sent error emails.
This appender does the following for repeated errors (the repetition/similarity recognition is configurable):
- the first occurrence is emailed immediately
- all following similar ERROR logs are buffered for a certain time (similarity and time is configurable)
- after the time passed, a summary email with summary info (number of events, time) and the first and last event is send

See also JavaDoc inside:
- [ExtendedSmtpAppender](src/main/java/org/apache/logging/log4j/core/appender/ExtendedSmtpAppender.java)
- Unit test [implementation](src/test/java/org/apache/logging/log4j/core/appender/ExtendedSmtpAppenderTest.java) and its [configuration](src/test/resources)


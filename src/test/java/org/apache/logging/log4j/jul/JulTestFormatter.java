package org.apache.logging.log4j.jul;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/** Copy of JDK 1.6 SimpleFormatter adjusted to desired format: "JUL:  %1$tT.%1$tL %4$s [%3$s: %2$s]  -  %5$s%6$s%n". */
public class JulTestFormatter extends Formatter {
    Date dat = new Date();
    private final static String format = "{0,time}";
    private MessageFormat formatter;

    private Object args[] = new Object[1];

    private final String lineSeparator = System.getProperty("line.separator");

    /**
     * Format the given LogRecord.
     * @param record the log record to be formatted.
     * @return a formatted log record
     */
    @Override
	public synchronized String format(LogRecord record) {
    	StringBuffer sb = new StringBuffer();
    	sb.append("JUL:  ");

    	// Minimize memory allocations here.
    	dat.setTime(record.getMillis());
    	args[0] = dat;
    	StringBuffer text = new StringBuffer();
    	if (formatter == null) {
    		formatter = new MessageFormat(format);
    	}
    	formatter.format(args, text, null);
    	sb.append(text);

    	sb.append(" ");
    	sb.append(record.getLevel().getLocalizedName());
    	sb.append(" [");
		sb.append(record.getLoggerName());
    	sb.append(": ");
    	if (record.getSourceClassName() != null) {
    		sb.append(record.getSourceClassName());
        	if (record.getSourceMethodName() != null) {
        		sb.append(" ");
        		sb.append(record.getSourceMethodName());
        	}
    	} else {
    		sb.append(record.getLoggerName());
    	}
    	sb.append("]  -  ");
    	sb.append(formatMessage(record));
    	sb.append(lineSeparator);
    	if (record.getThrown() != null) {
    		try {
    			StringWriter sw = new StringWriter();
    			PrintWriter pw = new PrintWriter(sw);
    			record.getThrown().printStackTrace(pw);
    			pw.close();
    			sb.append(sw.toString());
    		} catch (Exception ex) {
    		}
    	}
    	return sb.toString();
    }
}
